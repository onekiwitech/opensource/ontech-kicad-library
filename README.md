# ontech-kicad-library

### Config path

- **DIR_3DMODEL** : *path/ontek-kicad-library/3dmodels*
- **DIR_SHEET** : *path/ontek-kicad-library/sheet*

## KiPart
https://github.com/devbisme/KiPart

### Installation
- `pip install kipart`
- `kipart -s name nrf52840.xlsx -o nrf52840.lib`


## Schematic Field name Template
- Assembly Option
- Description
- Distributor
- Distributor Part#
- Manufacturer
- Manufacturer Part#

- Package
- Power
- Voltage
- Current
- Tolerance
- Material
- Function
